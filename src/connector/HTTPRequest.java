package connector;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTTPRequest {

    /**
     * Отправляет get к https://reqres.in/api/users/${args[0]}
     * @param urlToRead
     * @return
     * @throws Exception
     */

    public static String execGet(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
            return result.toString();


        }
    }

    /**
     * Метод parser принимает в себя ответ от https://reqres.in/api/users/${args[0]}
     * При помощи regex находит имя и фамилию клиента, и выводит их в return
     * @param json
     * @return
     */

    public static String parser(String json) {
        Matcher matcher = null;

        Pattern pattern = Pattern.compile("name\\\":\\\"(\\w*)\\\",\\\"\\w*\\\":\\\"(\\w*)");
        matcher = pattern.matcher(json);
        if(matcher.find()){
            return matcher.group(1)+" "+matcher.group(2);
        } else return "Returned empty value";


    }

}
