package reqres;

import connector.HTTPRequest;

import java.io.IOException;
import java.util.InputMismatchException;

/**
 * Приложение берёт первый аргумент и может в себя принять только целочисленный тип данных.
 * При вводе других типов данных появляется ошибка "No match found!"
 * При запуске с пустыми параметрами появляется ошибка "Please, insert the parameter."
 */

public class Reqres {
    public static void main (String[] args) throws Exception {
       try{
           String name = HTTPRequest.parser(HTTPRequest.execGet("https://reqres.in/api/users/"+args[0]));
           System.out.println(name);
       } catch (IllegalStateException e) {
           System.out.println("It's illegal.");
       } catch (InputMismatchException e){
           System.out.println("It can be integer only.");
       }catch (IOException e){
           System.out.println("No match found!");
       }catch (java.lang.ArrayIndexOutOfBoundsException e){
           System.out.println("Please, insert the parameter.");
       }



    }



}
